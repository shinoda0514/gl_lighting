//#version 130
in vec3 fN;// 輸出 Normal 在鏡頭座標下的方向
in vec3 fL;// 輸出 Light Direction 在鏡頭座標下的方向
in vec3 fE;// 輸出 View Direction 在鏡頭座標下的方向
in vec3 fSpotL;
in vec3 fSpotDir;
in vec3 fSpotL2;
in vec3 fSpotDir2;

// 以下為新增的部分
uniform vec4  LightInView;        // Light's position in View Space
uniform vec4  AmbientProduct;  // light's ambient  與 Object's ambient  與 ka 的乘積
uniform vec4  DiffuseProduct;  // light's diffuse  與 Object's diffuse  與 kd 的乘積
uniform vec4  SpecularProduct; // light's specular 與 Object's specular 與 ks 的乘積
uniform float fShininess;
uniform int   iLighting;
uniform vec4  vObjectColor;    // 代表物件的單一顏色

uniform int iPointSwitch;
uniform int iSpotSwitchI;
uniform int iSpotSwitchII;



vec4 PointLight()
{
	// 先宣告 diffuse 與 specular
	vec4 diffuse = vec4(0.0,0.0,0.0,1.0);
	vec4 specular = vec4(0.0,0.0,0.0,1.0);
	vec4 ambient = AmbientProduct;
	vec4 color ;
		
	if( iLighting != 1 ) {
			gl_FragColor = vObjectColor;
	}
	else {	
		// 1. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 單位化傳入的 Normal Dir
		vec3 vN = normalize(fN); 

		// 2. 單位化傳入的 Light Dir
		vec3 vL = normalize(fL); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0.0 ) { // 該點被光源照到才需要計算

			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct; 

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 單位化傳入的 fE , View Direction
			vec3 vV = normalize(fE);

			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;
			
			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess); 
		}
	}
	color = ambient + diffuse + specular;  // 計算顏色 ambient + diffuse + specular;
	color.w = 1.0;	// 設定 alpha 為 1.0	
	return(color);
}

vec4 SpotLight1()
{
	vec3 vSpotL = normalize(fSpotL);
	vec3 vSpotDir = normalize(fSpotDir);
	vec3 vN = normalize(fN); 
	vec4 diffuseSpot;
	vec4 specularSpot;
//	vec4 color = AmbientProduct;
	vec4 color = vec4(0.0,0.0,0.0,1.0);
	float dist = length(fSpotL);
	float constantAttenuation=1.0;
	float linearAttenuation=0.0;
	float quadraticAttenuation=0.0;
	float att,spotEffect;
	float spotExponent=1.0;
	float spotCosCutoff=0.99;
	float NdotSpotL = vN.x * vSpotL.x + vN.y * vSpotL.y + vN.z * vSpotL.z;
	vec3 vV=normalize(fE);
	vec3 halfV=normalize( vSpotL + vV );
	float NdotHV= vN.x*halfV.x + vN.y*halfV.y + vN.z*halfV.z;

	if(NdotSpotL > 0.0 ){//被照到
		spotEffect = dot( vSpotDir , -vSpotL );
		if(spotEffect > spotCosCutoff ){
			spotEffect = pow(spotEffect , spotExponent);
			att = spotEffect / (constantAttenuation + linearAttenuation * dist + quadraticAttenuation * dist * dist);
			color += att * (DiffuseProduct * NdotSpotL + AmbientProduct);
			color += att * SpecularProduct * pow(NdotHV,fShininess);
		}
		return(color);	
	}
	else return(vec4(0.0,0.0,0.0,1.0));
}

vec4 SpotLight2()
{
	vec3 vSpotL2 = normalize(fSpotL2);
	vec3 vSpotDir2 = normalize(fSpotDir2);
	vec3 vN = normalize(fN); 
	vec4 diffuseSpot;
	vec4 specularSpot;
//	vec4 color = AmbientProduct;
	vec4 color = vec4(0.0,0.0,0.0,1.0);
	float dist = length(fSpotL2);
	float constantAttenuation=1.0;
	float linearAttenuation=0.0;
	float quadraticAttenuation=0.0;
	float att,spotEffect;
	float spotExponent=1.0;
	float spotCosCutoff=0.99;
	float NdotSpotL2 = vN.x * vSpotL2.x + vN.y * vSpotL2.y + vN.z * vSpotL2.z;
	vec3 vV=normalize(fE);
	vec3 halfV=normalize( vSpotL2 + vV );
	float NdotHV= vN.x*halfV.x + vN.y*halfV.y + vN.z*halfV.z;

	if(NdotSpotL2 > 0.0 ){//被照到
		spotEffect = dot( vSpotDir2 , -vSpotL2 );
		if(spotEffect > spotCosCutoff ){
			spotEffect = pow(spotEffect , spotExponent);
			att = spotEffect / (constantAttenuation + linearAttenuation * dist + quadraticAttenuation * dist * dist);
			color += att * (DiffuseProduct * NdotSpotL2 + AmbientProduct);
			color += att * SpecularProduct * pow(NdotHV,fShininess);
		}
		return(color);	
	}
	else return(vec4(0.0,0.0,0.0,1.0));
}


void main()
{
	vec4 PointColor=vec4(0.0,0.0,0.0,1.0);
	vec4 SpotColor=vec4(0.0,0.0,0.0,1.0);
	vec4 Spot2Color=vec4(0.0,0.0,0.0,1.0);


	if(iPointSwitch==1)PointColor=PointLight();		

	if(iSpotSwitchI==1)SpotColor=SpotLight1();

	if(iSpotSwitchII==1)Spot2Color=SpotLight2();

	
	gl_FragColor=PointColor + SpotColor + Spot2Color;
	gl_FragColor.w = 1.0;
	
}
