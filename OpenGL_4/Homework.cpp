//v(5%)提供有別於 Cube 或 Sphere 的一個 (有意義的) 立體物件的繪製，而且必須支援 Per Vertex Lighting 與 Per Pixel Lighting 的計算
//必須產生相對應的 Vertex Normal 才算分數
//->CDiamond
//v(8%)實現至少三個光源的同時或個別的照明開關
//v(4%)實現 Spot Light 在 vsPerPixelLighting.glsl 中 
//得分:17%
//創意分數 : (3%)
//
//作業名稱:觀察真的很亮很漂亮的鑽石by PointLight & SpotLights
//使用說明:
//		有點光源、SpotLight1、SpotLight2
//		q(Q)鍵可以打開全部的燈、w(W)鍵可以關閉全部的燈
//		點光源的開關為z(Z)鍵、按a(A)鍵可控制是否要自動旋轉
//		SpotLigh1的開關為x(X)鍵、照射鑽石上部、座標在(0,10,0)
//		SpotLigh2的開關為c(C)鍵、照射鑽石下部、座標在(0,-15,0)
//		有兩種版本：
//			Version 1為兩顆SpotLight會自動轉動照明方向的
//				須把vsPerPixelLighting.glsl中第二行的#define VersionSpotLightMoveWithMouse設為註解
//				s(S)可以控制SpotLight1是否要自動旋轉
//				d(F)可以控制SpotLight2是否要自動旋轉
//			Version 2為兩顆SpotLight會隨著滑鼠移動改變照明位置
//				須在vsPerPixelLighting.glsl中第二行的#define VersionSpotLightMoveWithMouse

//
// 執行前的準備工作
// 關閉 CShape.h 中的 #define LIGHTING_WITHCPU 
// 開啟 CShape.h 中的 #define LIGHTING_WITHGPU 
// 關閉 CShape.h 中的 #define PERVERTEX_LIGHTING 
//

#include "header/Angel.h"
#include "Common/TypeDefine.h"
#include "Common/CDiamond.h"
#include "Common/CWireSphere.h"


#define SPACE_KEY 32
#define SCREEN_SIZE 800
#define HALF_SIZE SCREEN_SIZE /2 
#define VP_HALFWIDTH  20.0f
#define VP_HALFHEIGHT 20.0f


#define SETTING_MATERIALS 

// For Model View and Projection Matrix
mat4 g_mxModelView(1.0f);
mat4 g_mxProjection;

//Light Switches

int iPointSwitch=1;
int iSpotLightSwitchI=1;
int iSpotLightSwitchII=1;

// For Objects
CDiamond    *g_pDiamond;


// For View Point
GLfloat g_fRadius = 10.0;
GLfloat g_fTheta = 60.0f*DegreesToRadians;
GLfloat g_fPhi = 45.0f*DegreesToRadians;

point4  g_vEye( g_fRadius*sin(g_fTheta)*cos(g_fPhi), g_fRadius*sin(g_fTheta)*sin(g_fPhi), g_fRadius*cos(g_fTheta),  1.0 );
point4  g_vAt( 0.0, 0.0, 0.0, 1.0 );
vec4    g_vUp( 0.0, 1.0, 0.0, 0.0 );

//----------------------------------------------------------------------------
// Part 2 : for single light source
bool g_bAutoRotating = false;
bool g_bAutoRotatingSpot = false;
bool g_bAutoRotatingSpot2 = false;
float g_fElapsedTime = 0;
float g_fElapsedTimeSpot = 0;
float g_fElapsedTimeSpot2 = 0;
float g_fLightRadius = 10;
float g_fSpotLightDirRadius = 0.3f;
float g_fSpotLight2DirRadius = 0.15f;
float g_fLightTheta = 0;
float g_fSpotLightDirTheta = 0;
float g_fSpotLight2DirTheta = 0;

float g_fLightR = 0.95f;
float g_fLightG = 0.95f;
float g_fLightB = 0.95f;

structLightSource g_Light1 = {
    color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // ambient 
	color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // diffuse
	color4(g_fLightR, g_fLightG, g_fLightB, 1.0f), // specular
    point4(10.0f, 4.0f, 0.0f, 1.0f),   // position
    point4(0.0f, 0.0f, 0.0f, 1.0f),   // halfVector
    vec3(0.0f, 0.0f, 0.0f),			  //spotDirection
	1.0f	,	// spotExponent(parameter e); cos^(e)(phi) 
	45.0f,	// spotCutoff;	// (range: [0.0, 90.0], 180.0)  spot 的照明範圍
	1.0f	,	// spotCosCutoff; // (range: [1.0,0.0],-1.0), 照明方向與被照明點之間的角度取 cos 後, cut off 的值
	1	,	// constantAttenuation	(a + bd + cd^2)^-1 中的 a, d 為光源到被照明點的距離
	0	,	// linearAttenuation	    (a + bd + cd^2)^-1 中的 b
	0		// quadraticAttenuation (a + bd + cd^2)^-1 中的 c
};

CWireSphere *g_pLight;
vec4 g_SpotDir;
vec4 g_Spot2Dir;

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// 函式的原型宣告
extern void IdleProcess();

void init( void )
{
	mat4 mxT;
	vec4 vT, vColor;
	// 產生所需之 Model View 與 Projection Matrix

	g_mxModelView = LookAt( g_vEye, g_vAt, g_vUp );
	g_mxProjection = Perspective(60.0, (GLfloat)SCREEN_SIZE/(GLfloat)SCREEN_SIZE, 1.0, 1000.0);

	// 產生物件的實體

	g_pDiamond = new CDiamond(10.0);
// Part 3 : materials
#ifdef SETTING_MATERIALS
	g_pDiamond->SetMaterials(vec4(0.15f,0.15f,0.15f,1.0f), vec4(0.85f, 0, 0, 1), vec4(1.0f, 1.0f, 1.0f, 1.0f));
	g_pDiamond->SetKaKdKsShini(0.15f, 0.8f, 0.2f, 2);
#endif
	g_pDiamond->SetShader(g_mxModelView, g_mxProjection);
	// 設定 Cube
	vT.x = 1.5; vT.y = 1.5; vT.z = -1.5;
	mxT = Translate(vT);
	g_pDiamond->SetTRSMatrix(mxT);
	g_pDiamond->SetShadingMode(GOURAUD_SHADING);

	// 設定 代表 Light 的 WireSphere
	g_pLight = new CWireSphere(0.25f, 6, 3);
	g_pLight->SetShader(g_mxModelView, g_mxProjection);
	mxT = Translate(g_Light1.position);
	g_pLight->SetTRSMatrix(mxT);
	g_pLight->SetColor(g_Light1.diffuse);
#ifdef LIGHTING_WITHGPU
	g_pLight->SetLightingDisable();
#endif
}

//----------------------------------------------------------------------------
void GL_Display( void )
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // clear the window

	g_pDiamond->Draw();
	g_pLight->Draw();

	glutSwapBuffers();	// 交換 Frame Buffer
}

//----------------------------------------------------------------------------
// Part 2 : for single light source
void UpdateLightPosition(float dt)
{
	mat4 mxT;
	// 每秒繞 Y 軸轉 90 度
	g_fElapsedTime += dt;
	g_fLightTheta = g_fElapsedTime*(float)M_PI_2;
	if( g_fLightTheta >= (float)M_PI*2.0f ) {
		g_fLightTheta -= (float)M_PI*2.0f;
		g_fElapsedTime -= 4.0f;
	}
	g_Light1.position.x = g_fLightRadius * cosf(g_fLightTheta);
	g_Light1.position.z = g_fLightRadius * sinf(g_fLightTheta);
	mxT = Translate(g_Light1.position);
	g_pLight->SetTRSMatrix(mxT);
}
void UpdateSpotLightDir(float dt)
{

	// 每秒繞 Y 軸轉 90 度
	g_fElapsedTimeSpot += dt;
	g_fSpotLightDirTheta = g_fElapsedTimeSpot*(float)M_PI_2;
	if( g_fSpotLightDirTheta >= (float)M_PI*2.0f ) {
		g_fSpotLightDirTheta -= (float)M_PI*2.0f;
		g_fElapsedTimeSpot -= 4.0f;
	}
	g_pDiamond->SetSpotDir(g_fSpotLightDirRadius * cosf(g_fSpotLightDirTheta) , g_fSpotLightDirRadius * sinf(g_fSpotLightDirTheta));

}
void UpdateSpotLight2Dir(float dt)
{

	// 每秒繞 Y 軸轉 90 度
	g_fElapsedTimeSpot2 += dt;
	g_fSpotLight2DirTheta = g_fElapsedTimeSpot2*(float)M_PI_2;
	if( g_fSpotLight2DirTheta >= (float)M_PI*2.0f ) {
		g_fSpotLight2DirTheta -= (float)M_PI*2.0f;
		g_fElapsedTimeSpot2 -= 4.0f;
	}
	g_pDiamond->SetSpot2Dir(g_fSpotLight2DirRadius * cosf(g_fSpotLight2DirTheta) , g_fSpotLight2DirRadius * sinf(g_fSpotLight2DirTheta));

}
//----------------------------------------------------------------------------

void onFrameMove(float delta)
{

	if( g_bAutoRotating ) { // Part 2 : 重新計算 Light 的位置
		UpdateLightPosition(delta);
	}

	if( g_bAutoRotatingSpot ) { // Part 2 : 重新計算 Light 的位置
		UpdateSpotLightDir(delta);
	}

	if( g_bAutoRotatingSpot2 ) { // Part 2 : 重新計算 Light 的位置
		UpdateSpotLight2Dir(delta);
	}
	// 如果需要重新計算時，在這邊計算每一個物件的顏色

	g_pDiamond->Update(delta, g_Light1);
	g_pLight->Update(delta);


	g_pDiamond->SetSwitches(iPointSwitch, iSpotLightSwitchI,iSpotLightSwitchII);
	g_pLight->SetSwitches(iPointSwitch, iSpotLightSwitchI,iSpotLightSwitchII);

	GL_Display();
}

//----------------------------------------------------------------------------

void Win_Keyboard( unsigned char key, int x, int y )
{
    switch ( key ) {
	case  SPACE_KEY:

		break;
//----------------------------------------------------------------------------
// Part 2 : for single light source
	case 81: // Q key
	case 113: // q key
		iPointSwitch=1;
		iSpotLightSwitchI=1;
		iSpotLightSwitchII=1;
	break;

	case 87: // W key
	case 119: // w key
		iPointSwitch=0;
		iSpotLightSwitchI=0;
		iSpotLightSwitchII=0;
	break;
	
	case 90: // Z key
	case 122: // z key
		if(iPointSwitch==1)iPointSwitch=0;
		else iPointSwitch=1;
	break;

	case 88: // X key
	case 120: // x key
		if(iSpotLightSwitchI==1)iSpotLightSwitchI=0;
		else iSpotLightSwitchI=1;
	break;

	case 67: // C key
	case 99: // c key
		if(iSpotLightSwitchII==1)iSpotLightSwitchII=0;
		else iSpotLightSwitchII=1;
	break;
	
	case 65: // A key
	case 97: // a key
		g_bAutoRotating = !g_bAutoRotating;
	break;

	case 83: // S key
	case 115: // s key
		g_bAutoRotatingSpot = !g_bAutoRotatingSpot;
	break;

	case 68: // D key
	case 100: // d key
		g_bAutoRotatingSpot2 = !g_bAutoRotatingSpot2;
	break;
//----------------------------------------------------------------------------
    case 033:
		glutIdleFunc( NULL );
		delete g_pDiamond;
		delete g_pLight;
        exit( EXIT_SUCCESS );
        break;
    }
}

//----------------------------------------------------------------------------
void Win_Mouse(int button, int state, int x, int y) {
	switch(button) {
		case GLUT_LEFT_BUTTON:   // 目前按下的是滑鼠左鍵
			//if ( state == GLUT_DOWN ) ; 
			break;
		case GLUT_MIDDLE_BUTTON:  // 目前按下的是滑鼠中鍵 ，換成 Y 軸
			//if ( state == GLUT_DOWN ) ; 
			break;
		case GLUT_RIGHT_BUTTON:   // 目前按下的是滑鼠右鍵
			//if ( state == GLUT_DOWN ) ;
			break;
		default:
			break;
	} 
}
//----------------------------------------------------------------------------
void Win_SpecialKeyboard(int key, int x, int y) {

	switch(key) {
		case GLUT_KEY_LEFT:		// 目前按下的是向左方向鍵

			break;
		case GLUT_KEY_RIGHT:	// 目前按下的是向右方向鍵

			break;
		default:
			break;
	}
}

//----------------------------------------------------------------------------
// The passive motion callback for a window is called when the mouse moves within the window while no mouse buttons are pressed.
void Win_PassiveMotion(int x, int y) {

	g_fPhi = (float)-M_PI*(x - HALF_SIZE)/(HALF_SIZE); // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
	g_fTheta = (float)M_PI*(float)y/SCREEN_SIZE;
	g_vEye.x = g_fRadius*sin(g_fTheta)*sin(g_fPhi);
	g_vEye.y = g_fRadius*cos(g_fTheta);
	g_vEye.z = g_fRadius*sin(g_fTheta)*cos(g_fPhi);

	g_mxModelView = LookAt( g_vEye, g_vAt, g_vUp );

	// Change ModelView Matrix
	g_pDiamond->SetModelViewMatrix(g_mxModelView);
	g_pLight->SetModelViewMatrix(g_mxModelView);

	//glutPostRedisplay();
}

// The motion callback for a window is called when the mouse moves within the window while one or more mouse buttons are pressed.
void Win_MouseMotion(int x, int y) {
	g_fPhi = (float)-M_PI*(x - HALF_SIZE)/(HALF_SIZE);  // 轉換成 g_fPhi 介於 -PI 到 PI 之間 (-180 ~ 180 之間)
	g_fTheta = (float)M_PI*(float)y/SCREEN_SIZE;;
	g_vEye.x = g_fRadius*sin(g_fTheta)*sin(g_fPhi);
	g_vEye.y = g_fRadius*cos(g_fTheta);
	g_vEye.z = g_fRadius*sin(g_fTheta)*cos(g_fPhi);

	g_mxModelView = LookAt( g_vEye, g_vAt, g_vUp );

	// Change ModelView Matrix
	g_pDiamond->SetModelViewMatrix(g_mxModelView);
	g_pLight->SetModelViewMatrix(g_mxModelView);
	//glutPostRedisplay();
}
//----------------------------------------------------------------------------
void GL_Reshape(GLsizei w, GLsizei h)
{
	glViewport(0, 0, w, h);

	//  產生 projection 矩陣，此處為產生正投影矩陣
	g_mxProjection = Perspective(60.0, (GLfloat)w/(GLfloat)h, 1.0, 1000.0);

	g_pDiamond->SetProjectionMatrix(g_mxProjection);
	g_pLight->SetProjectionMatrix(g_mxProjection); 

	glClearColor( 0.0, 0.0, 0.0, 1.0 ); // black background
	glEnable(GL_DEPTH_TEST);
}

//----------------------------------------------------------------------------

int main( int argc, char **argv )
{
    
	glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DOUBLE );
    glutInitWindowSize( SCREEN_SIZE, SCREEN_SIZE );

	// If you use freeglut the two lines of code can be added to your application 
	glutInitContextVersion( 3, 2 );
	glutInitContextProfile( GLUT_CORE_PROFILE );

    glutCreateWindow("Shading Example 3");

	// The glewExperimental global switch can be turned on by setting it to GL_TRUE before calling glewInit(), 
	// which ensures that all extensions with valid entry points will be exposed.
	glewExperimental = GL_TRUE; 
    glewInit();  

    init();

	glutMouseFunc(Win_Mouse);
	glutMotionFunc(Win_MouseMotion);
	glutPassiveMotionFunc(Win_PassiveMotion);  
    glutKeyboardFunc( Win_Keyboard );	// 處理 ASCI 按鍵如 A、a、ESC 鍵...等等
	glutSpecialFunc( Win_SpecialKeyboard);	// 處理 NON-ASCI 按鍵如 F1、Home、方向鍵...等等
    glutDisplayFunc( GL_Display );
	glutReshapeFunc( GL_Reshape );
	glutIdleFunc( IdleProcess );
	
    glutMainLoop();
    return 0;
}