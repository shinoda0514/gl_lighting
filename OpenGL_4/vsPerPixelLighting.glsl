// Phong reflection model
#version 130
//#define VersionSpotLightMoveWithMouse

in vec4 vPosition;	  // Vertex Position
in vec3 vNormal;    // Vertex Normal
in vec4 vVtxColor;  // Vertex Color

out vec3 fN;// 輸出 Normal 在鏡頭座標下的方向
out vec3 fL;// 輸出 Light Direction 在鏡頭座標下的方向
out vec3 fE;// 輸出 View Direction 在鏡頭座標下的方向
out vec3 fSpotL;
out vec3 fSpotDir;
out vec3 fSpotL2;
out vec3 fSpotDir2;


uniform mat4  ModelView;   // Model View Matrix
uniform mat4  Projection;  // Projection Matrix
uniform vec4  LightInView; // Light's position in View Space
uniform vec4 SpotLightDir;
uniform vec4 SpotLight2Dir;

void main()
{
	vec4 vPosInView = ModelView * vPosition;
	// 目前已經以鏡頭座標為基礎, 所以 View 的位置就是 (0,0,0), 所以位置的富項就是 View Dir
	fE = -vPosInView.xyz;

	// 以下兩行用於計算對物件進行 non-uniform scale 時，物件 normal 的修正計算
	//		mat3 ITModelView = transpose(inverse(mat3(ModelView)); 
	//		vec3 vN = normalize(ITModelView * vNormal); 
	fN = (ModelView * vec4(vNormal, 0.0)).xyz;

	fL = vec3(LightInView.xyz - vPosInView.xyz);
	gl_Position = Projection * vPosInView;

#ifdef VersionSpotLightMoveWithMouse
	fSpotL= (ModelView * (vec4(0.0,10.0,0.0,1.0)-vPosition )).xyz;//預設光的座標為(0,10,0)
	fSpotDir=(ModelView * vec4(0.0,-1.0,0.0,1.0)).xyz;//預設光向-y方向照射->(0,-1,0)
	fSpotL2= (ModelView * (vec4(0.0,-20.0,0.0,1.0)-vPosition )).xyz;//預設光的座標為(0,10,0)
	fSpotDir2=(ModelView * vec4(1.0,1.0,0.0,1.0)).xyz;//預設光向-y方向照射->(0,-1,0)
#else
	fSpotL= (ModelView * (vec4(0.0,10.0,0.0,1.0)-vPosition )).xyz;//預設光的座標為(0,10,0)
	fSpotDir=(ModelView * vec4(SpotLightDir.x,-1.0,SpotLightDir.z,0.0)).xyz;//預設光向-y方向照射->(0.3,-1,0)
	fSpotL2= (ModelView * (vec4(0.0,-15.0,0.0,1.0)-vPosition )).xyz;//預設光的座標為(0,10,0)
	fSpotDir2=(ModelView * vec4(SpotLight2Dir.x,+1.0,SpotLight2Dir.z,0.0)).xyz;//預設光向-y方向照射->(0.3,-1,0)
#endif
}
